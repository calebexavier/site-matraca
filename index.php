<?php
require __DIR__ . "/vendor/autoload.php";

use CoffeeCode\Router\Router;

$router = new Router(BASE_URL);

/*
 * Controllers
 */

$router->namespace("Source\App");

/*
 * Web
 * home
 */

$router->group(null);
$router->get("/","Web:Home");
$router->get("/ver-produto/{id}","Web:VerProduto");
$router->get("/painel-cliente","Web:painelCliente");
$router->get("/carrinho","Web:carrinho");
$router->get("/checkout","Web:checkout");
//$router->get("/{filter}/{page}","Web:Home");

/*
 * Web
 * Página inical
 */

$router->group(null);
$router->get("/dashboard","Web:Dashboard");


/*
 * Web
 * Contas
 */

$router->group(null);
$router->get("/cadastrar-usuario","Web:CadastrarUsuario");
$router->get("/listar-usuarios","Web:ListarUsuarios");
$router->get("/editar-usuario/{id}","Web:EditarUsuario");

$router->get("/cadastrar-produto","Web:CadastrarProduto");
$router->get("/listar-produtos","Web:ListarProdutos");
$router->get("/editar-produto/{id}","Web:EditarProduto");

/*
 * ERRORS
 */
$router->group("ooops");
$router->get("/{errcode}","Web:Error");

$router->dispatch();

if ($router->error()) {
    $router->redirect("/../ooops/{$router->error()}");
}
