<?php

namespace Source\App;

class Web
{
    public function home($data)
    {
        $page = 'pagina-inicial';
        
        require_once __DIR__.'/pages/index-1.php';
    }

    public function error($data)
    {
        
        require_once '../dashboard/pages/not-found.php';
        //echo BASE_URL;
    }

    public function VerProduto($data)
    {
        $page = 'ver-produto';
        require_once __DIR__.'/pages/ver-produto.php';
    }

    public function painelCliente($data)
    {
        $page = 'painel-cliente';
        require_once __DIR__.'/pages/painel-cliente.php';
    }

    public function carrinho($data)
    {
        $page = 'carrinho';
        require_once __DIR__.'/pages/cart.php';
    }

    public function checkout($data)
    {
        $page = 'checkout';
        require_once __DIR__.'/pages/checkout.php';
    }



    public function listarUsuarios($data)
    {
        $page = 'listar-usuarios';
        require_once __DIR__.'/pages/listar-usuarios.php';
    }

    public function editarUsuario($data)
    {
        $page = 'contas-bloqueadas';
        $id=$data['id'];
        require_once __DIR__.'/pages/editar-usuario.php';
    }
    public function cadastrarProduto($data)
    {
        $page = 'cadastrar-produto';
        require_once __DIR__.'/pages/cadastrar-produto.php';
    }

    public function listarProdutos($data)
    {
        $page = 'listar-produtos';
        require_once __DIR__.'/pages/listar-produtos.php';
    }

    public function editarProduto($data)
    {
        $page = 'editar-produto';
        $id=$data['id'];
        require_once __DIR__.'/pages/editar-produto.php';
    }

}
