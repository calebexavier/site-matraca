﻿<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= APP_NAME ?></title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
          content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=BASE_URL?>\source\App\files\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>\source\App\files\bower_components\bootstrap\css\bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>\source\App\files\assets\icon\feather\css\feather.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>\source\App\files\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>\source\App\files\assets\css\jquery.mCustomScrollbar.css">
</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <!-- header -->
        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">

                <?php
                require_once("includes/logo.php");
                ?>

            </div>
        </nav>


        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <!-- Menus Laterais -->
                <?php
                require_once('includes/sidebar.php');
                ?>
                <!-- Fim menu lateral -->
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">

                                <div class="page-body">
                                    <div class="row">
                                        <!-- task, page, download counter  start -->
                                        <div class="col-xl-3 col-md-6">
                                            <div class="card bg-c-yellow update-card">
                                                <div class="card-block">
                                                    <div class="row align-items-end">
                                                        <div class="col-8">
                                                            <h4 class="text-white">04*</h4>
                                                            <h6 class="text-white m-b-0">Usuários</h6>
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <canvas id="update-chart-1" height="50"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <p class="text-white m-b-0"><i
                                                                class="feather icon-clock text-white f-14 m-r-10"></i>atualizado
                                                        : 9:15 am</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="card bg-c-green update-card">
                                                <div class="card-block">
                                                    <div class="row align-items-end">
                                                        <div class="col-8">
                                                            <h4 class="text-white">40*</h4>
                                                            <h6 class="text-white m-b-0">Produtos</h6>
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <canvas id="update-chart-2" height="50"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <p class="text-white m-b-0"><i
                                                                class="feather icon-clock text-white f-14 m-r-10"></i>atualizado
                                                        : 9:15 am</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="card bg-c-pink update-card">
                                                <div class="card-block">
                                                    <div class="row align-items-end">
                                                        <div class="col-8">
                                                            <h4 class="text-white">05*</h4>
                                                            <h6 class="text-white m-b-0">Vendas hoje</h6>
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <canvas id="update-chart-3" height="50"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <p class="text-white m-b-0"><i
                                                                class="feather icon-clock text-white f-14 m-r-10"></i>atualizado
                                                        : 9:15 am</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="card bg-c-lite-green update-card">
                                                <div class="card-block">
                                                    <div class="row align-items-end">
                                                        <div class="col-8">
                                                            <h4 class="text-white">80*</h4>
                                                            <h6 class="text-white m-b-0">Total de vendas</h6>
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <canvas id="update-chart-4" height="50"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <p class="text-white m-b-0"><i
                                                                class="feather icon-clock text-white f-14 m-r-10"></i>atualizado
                                                        : 9:15 am</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- task, page, download counter  end -->


                                        <!-- card bem vindo -->
                                        <div class="col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Bem vindo </h5>
                                                    <span class="text-muted">Olá matraca  .</span>
                                                </div>
                                                <div class="card-block">
                                                    O objetivo desse painel te guiar diante das operações financeiras e administrativas
                                                    que podem ser feitas . <br/> O menu ao lado esquerdo tem todas as operações disponíveis até o momento

                                                </div>

                                            </div>
                                        </div>
                                        <!-- card bem vindo -->

                                        <!-- card bem vindo -->
                                        <div class="col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>* Dados meramente ilustrativos </h5>

                                                </div>
                                                <div class="card-block">

                                                </div>

                                            </div>
                                        </div>
                                        <!-- card bem vindo -->



                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fim sidebar -->
    </div>
</div>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="../files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\jquery\js\jquery.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\bootstrap\js\bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\modernizr\js\modernizr.js"></script>
<!-- Chart js -->
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\bower_components\chart.js\js\Chart.js"></script>
<!-- amchart js -->
<script src="<?=BASE_URL?>\source\App\files\assets\pages\widget\amchart\amcharts.js"></script>
<script src="<?=BASE_URL?>\source\App\files\assets\pages\widget\amchart\serial.js"></script>
<script src="<?=BASE_URL?>\source\App\files\assets\pages\widget\amchart\light.js"></script>
<script src="<?=BASE_URL?>\source\App\files\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\assets\js\SmoothScroll.js"></script>
<script src="<?=BASE_URL?>\source\App\files\assets\js\pcoded.min.js"></script>
<!-- custom js -->
<script src="<?=BASE_URL?>\source\App\files\assets\js\vartical-layout.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\assets\js\script.min.js"></script>
<script type="text/javascript" src="<?=BASE_URL?>\source\App\files\assets\js\protected-pages.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>


    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>

</html>
