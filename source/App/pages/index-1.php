<!DOCTYPE html>
<html lang="en">


<!-- molla/index-1.html  22 Nov 2019 09:55:06 GMT -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Matraca - eCommerce</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Matraca - eCommerce">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <link rel="manifest" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/site.html">
    <link rel="mask-icon" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/safari-pinned-tab.svg" color="#666666">
    <link rel="shortcut icon" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="<?= BASE_URL ?>/source/App/pages/assets/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/plugins/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/plugins/jquery.countdown.css">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/style.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/skins/skin-demo-2.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/demos/demo-2.css">
</head>

<body>
    <div class="page-wrapper">
        <?php require_once __DIR__ . "/includes/header.php"; ?>

        <main class="main">

            <div class="intro-slider-container">
                <div class="owl-carousel owl-simple owl-light owl-nav-inside" data-toggle="owl" data-owl-options='{"nav": false}'>
                    <div class="intro-slide" style="background-image: url(<?= BASE_URL ?>/source/App/pages/assets/images/banners/Vape_BannerTopo02.jpg);">
                        <div class="container intro-content">
                            <h3 class="intro-subtitle">Mobília de quarto</h3><!-- End .h3 intro-subtitle -->
                            <h1 class="intro-title">Encontre o conforto <br>Que você precisa.</h1><!-- End .intro-title -->

                            <a href="category.html" class="btn btn-primary">
                                <span>Compre agora</span>
                                <i class="icon-long-arrow-right"></i>
                            </a>
                        </div><!-- End .container intro-content -->
                    </div><!-- End .intro-slide -->

                    <div class="intro-slide" style="background-image: url(<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/slider/slide-2.jpg);">
                        <div class="container intro-content">
                            <h3 class="intro-subtitle">Deals and Promotions</h3><!-- End .h3 intro-subtitle -->
                            <h1 class="intro-title">Ypperlig <br>Coffee Table <br><span class="text-primary"><sup>$</sup>49,99</span></h1><!-- End .intro-title -->

                            <a href="category.html" class="btn btn-primary">
                                <span>Shop Now</span>
                                <i class="icon-long-arrow-right"></i>
                            </a>
                        </div><!-- End .container intro-content -->
                    </div><!-- End .intro-slide -->

                    <div class="intro-slide" style="background-image: url(<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/slider/slide-3.jpg);">
                        <div class="container intro-content">
                            <h3 class="intro-subtitle">Living Room</h3><!-- End .h3 intro-subtitle -->
                            <h1 class="intro-title">
                                Make Your Living Room <br>Work For You.<br>
                                <span class="text-primary">
                                    <sup class="text-white font-weight-light">from</sup><sup>$</sup>9,99
                                </span>
                            </h1><!-- End .intro-title -->

                            <a href="category.html" class="btn btn-primary">
                                <span>Shop Now</span>
                                <i class="icon-long-arrow-right"></i>
                            </a>
                        </div><!-- End .container intro-content -->
                    </div><!-- End .intro-slide -->
                </div><!-- End .owl-carousel owl-simple -->

                <span class="slider-loader text-white"></span><!-- End .slider-loader -->
            </div><!-- End .intro-slider-container -->

            <!--

            <div class="brands-border owl-carousel owl-simple" data-toggle="owl" data-owl-options='{
                    "nav": false, 
                    "dots": false,
                    "margin": 0,
                    "loop": false,
                    "responsive": {
                        "0": {
                            "items":2
                        },
                        "420": {
                            "items":3
                        },
                        "600": {
                            "items":4
                        },
                        "900": {
                            "items":5
                        },
                        "1024": {
                            "items":6
                        },
                        "1360": {
                            "items":7
                        }
                    }
                }'>
                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/1.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/2.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/3.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/4.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/5.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/6.png" alt="Brand Name">
                </a>

                <a href="#" class="brand">
                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/brands/7.png" alt="Brand Name">
                </a>
            </div><!-- End .owl-carousel -->

            <div class="mb-3 mb-lg-5"></div><!-- End .mb-3 mb-lg-5 -->

            <div class="text-center h4 pb-3">Conheça</div>

            <div class="banner-group">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-5">
                            <div class="banner banner-large banner-overlay banner-overlay-light">
                                <a href="#">
                                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/banners/CoffeTables.jpg" alt="Banner">
                                </a>
                                <!--

                                <div class="banner-content banner-content-top">
                                    <h4 class="banner-subtitle">Driven - CBD</h4><!-- End .banner-subtitle 
                                    <h3 class="banner-title">Vape Juice</h3><!-- End .banner-title 
                                    <div class="banner-text"> R$120</div><!-- End .banner-text 
                                    <a href="#" class="btn btn-outline-gray banner-link">Compre Agora<i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .banner-content -->
                            </div><!-- End .banner -->
                        </div><!-- End .col-lg-5 -->

                        <div class="col-md-6 col-lg-3">
                            <div class="banner banner-overlay">
                                <a href="#">
                                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/banners/AmazingArmchairs.jpg" alt="Banner">
                                </a>
                                <!--

                                <div class="banner-content banner-content-bottom">
                                    <h4 class="banner-subtitle text-grey">On Sale</h4><!-- End .banner-subtitle 
                                    <h3 class="banner-title text-white">Amazing <br>Armchairs</h3><!-- End .banner-title 
                                    <div class="banner-text text-white">from $39.99</div><!-- End .banner-text
                                    <a href="#" class="btn btn-outline-white banner-link">Discover Now<i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .banner-content -->
                            </div><!-- End .banner -->
                        </div><!-- End .col-lg-3 -->

                        <div class="col-md-6 col-lg-4">
                            <div class="banner banner-overlay">
                                <a href="#">
                                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/banners/StorageBoxes.jpg" alt="Banner">
                                </a>
                                <!--

                                <div class="banner-content banner-content-top">
                                    <h4 class="banner-subtitle text-grey">New Arrivals</h4><!-- End .banner-subtitle 
                                    <h3 class="banner-title text-white">Storage <br>Boxes & Baskets</h3><!-- End .banner-title
                                    <a href="#" class="btn btn-outline-white banner-link">Discover Now<i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .banner-content -->
                            </div><!-- End .banner -->

                            <div class="banner banner-overlay banner-overlay-light">
                                <a href="#">
                                    <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/banners/OnSaleLamps.jpg" alt="Banner">
                                </a>
                                <!--
                                <div class="banner-content banner-content-top">
                                    <h4 class="banner-subtitle">On Sale</h4><!-- End .banner-subtitle 
                                    <h3 class="banner-title">Lamps Offer</h3><!-- End .banner-title
                                    <div class="banner-text">up to 30% off</div><!-- End .banner-text
                                    <a href="#" class="btn btn-outline-gray banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .banner-content -->
                            </div><!-- End .banner -->
                        </div><!-- End .col-lg-4 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .banner-group -->

            <div class="mb-3"></div><!-- End .mb-6 -->

            <div class="container">
                <ul class="nav nav-pills nav-border-anim nav-big justify-content-center mb-3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="products-featured-link" data-toggle="tab" href="#products-featured-tab" role="tab" aria-controls="products-featured-tab" aria-selected="true">Destaques</a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link" id="products-sale-link" data-toggle="tab" href="#products-sale-tab" role="tab" aria-controls="products-sale-tab" aria-selected="false">On Sale</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="products-top-link" data-toggle="tab" href="#products-top-tab" role="tab" aria-controls="products-top-tab" aria-selected="false">Top Rated</a>
                    </li>
                    --
                </ul>
            </div><!-- End .container -->

                    <div class="container-fluid">
                        <div class="tab-content tab-content-carousel">
                            <div class="tab-pane p-0 fade show active" id="products-featured-tab" role="tabpanel" aria-labelledby="products-featured-link">
                                <div id="owl-demo-1" class="">
                                </div>
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane p-0 fade" id="products-sale-tab" role="tabpanel" aria-labelledby="products-sale-link">
                                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-5-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-5-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->

                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Petite Table Lamp</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                $675,00
                                            </div><!-- End .product-price -->

                                            <div class="product-nav product-nav-dots">
                                                <a href="#" class="active" style="background: #74543e;"><span class="sr-only">Color name</span></a>
                                                <a href="#" style="background: #e8e8e8;"><span class="sr-only">Color name</span></a>
                                            </div><!-- End .product-nav -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->

                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-6-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-6-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->

                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Elephant Armchair</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                $457,00
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->

                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-1-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-1-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->

                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Butler Stool Ladder</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                $251,00
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->
                                </div><!-- End .owl-carousel -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane p-0 fade" id="products-top-tab" role="tabpanel" aria-labelledby="products-top-link">
                                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":5
                                    },
                                    "1600": {
                                        "items":6,
                                        "nav": true
                                    }
                                }
                            }'>
                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-2-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-2-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->
                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Octo 4240</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                $746,00
                                            </div><!-- End .product-price -->

                                            <div class="product-nav product-nav-dots">
                                                <a href="#" class="active" style="background: #1f1e18;"><span class="sr-only">Color name</span></a>
                                                <a href="#" style="background: #e8e8e8;"><span class="sr-only">Color name</span></a>
                                            </div><!-- End .product-nav -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->

                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <span class="product-label label-circle label-new">New</span>
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-3-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-3-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->

                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Flow Slim Armchair</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                $970,00
                                            </div><!-- End .product-price -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->

                                    <div class="product product-11 text-center">
                                        <figure class="product-media">
                                            <span class="product-label label-circle label-sale">Sale</span>
                                            <a href="product.html">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-4-1.jpg" alt="Product image" class="product-image">
                                                <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-4-2.jpg" alt="Product image" class="product-image-hover">
                                            </a>

                                            <div class="product-action-vertical">
                                                <a href="#" class="btn-product-icon btn-wishlist"><span>add to wishlist</span></a>
                                            </div><!-- End .product-action-vertical -->

                                        </figure><!-- End .product-media -->

                                        <div class="product-body">
                                            <h3 class="product-title"><a href="product.html">Roots Sofa Bed</a></h3><!-- End .product-title -->
                                            <div class="product-price">
                                                <span class="new-price">$337,00</span>
                                                <span class="old-price">Was $449,00</span>
                                            </div><!-- End .product-price -->

                                            <div class="product-nav product-nav-dots">
                                                <a href="#" class="active" style="background: #878883;"><span class="sr-only">Color name</span></a>
                                                <a href="#" style="background: #dfd5c2;"><span class="sr-only">Color name</span></a>
                                            </div><!-- End .product-nav -->
                                        </div><!-- End .product-body -->
                                        <div class="product-action">
                                            <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                        </div><!-- End .product-action -->
                                    </div><!-- End .product -->
                                </div><!-- End .owl-carousel -->
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .container-fluid -->

                    <div class="mb-5"></div><!-- End .mb-5 -->




                    <div class="mb-6"></div><!-- End .mb-6 -->

                    <div class="container d-none">
                        <div class="heading heading-center mb-3">
                            <h2 class="title">Top Selling Products</h2><!-- End .title -->

                            <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="top-all-link" data-toggle="tab" href="#top-all-tab" role="tab" aria-controls="top-all-tab" aria-selected="true">All</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="top-fur-link" data-toggle="tab" href="#top-fur-tab" role="tab" aria-controls="top-fur-tab" aria-selected="false">Furniture</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="top-decor-link" data-toggle="tab" href="#top-decor-tab" role="tab" aria-controls="top-decor-tab" aria-selected="false">Decoration</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="top-light-link" data-toggle="tab" href="#top-light-tab" role="tab" aria-controls="top-light-tab" aria-selected="false">Lighting</a>
                                </li>
                            </ul>
                        </div><!-- End .heading -->

                        <div class="tab-content">
                            <div class="tab-pane p-0 fade show active" id="top-all-tab" role="tabpanel" aria-labelledby="top-all-link">
                                <div class="products">
                                    <div class="row justify-content-center">
                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-7-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-7-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Petite Table Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-8-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-8-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Madra Log Holder</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->

                                                    <div class="product-nav product-nav-dots">
                                                        <a href="#" class="active" style="background: #333333;"><span class="sr-only">Color name</span></a>
                                                        <a href="#" style="background: #927764;"><span class="sr-only">Color name</span></a>
                                                    </div><!-- End .product-nav -->

                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <span class="product-label label-circle label-sale">Sale</span>
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-9-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-9-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Garden Armchair</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        <span class="new-price">$94,00</span>
                                                        <span class="old-price">Was $94,00</span>
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-10-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-10-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Carronade Large Suspension Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->

                                                    <div class="product-nav product-nav-dots">
                                                        <a href="#" class="active" style="background: #e8e8e8;"><span class="sr-only">Color name</span></a>
                                                        <a href="#" style="background: #333333;"><span class="sr-only">Color name</span></a>
                                                    </div><!-- End .product-nav -->

                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-11-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-11-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Original Outdoor Beanbag</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $259,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <span class="product-label label-circle label-new">New</span>
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-12-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-12-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">2-Seater</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $3.107,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-13-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-13-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Wingback Chair</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $2.486,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-14-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-14-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Cushion Set 3 Pieces</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $199,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-15-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-15-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Cushion Set 3 Pieces</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $199,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-16-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-16-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Carronade Table Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $499,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                                    </div><!-- End .row -->
                                </div><!-- End .products -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane p-0 fade" id="top-fur-tab" role="tabpanel" aria-labelledby="top-fur-link">
                                <div class="products">
                                    <div class="row justify-content-center">
                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <span class="product-label label-circle label-sale">Sale</span>
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-9-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-9-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Garden Armchair</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        <span class="new-price">$94,00</span>
                                                        <span class="old-price">Was $94,00</span>
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <span class="product-label label-circle label-new">New</span>
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-12-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-12-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">2-Seater</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $3.107,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-13-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-13-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Furniture</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Wingback Chair</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $2.486,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                                    </div><!-- End .row -->
                                </div><!-- End .products -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane p-0 fade" id="top-decor-tab" role="tabpanel" aria-labelledby="top-decor-link">
                                <div class="products">
                                    <div class="row justify-content-center">
                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-8-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-8-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Madra Log Holder</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->

                                                    <div class="product-nav product-nav-dots">
                                                        <a href="#" class="active" style="background: #333333;"><span class="sr-only">Color name</span></a>
                                                        <a href="#" style="background: #927764;"><span class="sr-only">Color name</span></a>
                                                    </div><!-- End .product-nav -->

                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-11-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-11-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Original Outdoor Beanbag</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $259,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-14-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-14-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Cushion Set 3 Pieces</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $199,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-15-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-15-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Decor</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Cushion Set 3 Pieces</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $199,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                                    </div><!-- End .row -->
                                </div><!-- End .products -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane p-0 fade" id="top-light-tab" role="tabpanel" aria-labelledby="top-light-link">
                                <div class="products">
                                    <div class="row justify-content-center">
                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-7-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-7-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Petite Table Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-10-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-10-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Carronade Large Suspension Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $401,00
                                                    </div><!-- End .product-price -->

                                                    <div class="product-nav product-nav-dots">
                                                        <a href="#" class="active" style="background: #e8e8e8;"><span class="sr-only">Color name</span></a>
                                                        <a href="#" style="background: #333333;"><span class="sr-only">Color name</span></a>
                                                    </div><!-- End .product-nav -->

                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->

                                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                                            <div class="product product-11 text-center">
                                                <figure class="product-media">
                                                    <a href="product.html">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-16-1.jpg" alt="Product image" class="product-image">
                                                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-16-2.jpg" alt="Product image" class="product-image-hover">
                                                    </a>

                                                    <div class="product-action-vertical">
                                                        <a href="#" class="btn-product-icon btn-wishlist "><span>add to wishlist</span></a>
                                                    </div><!-- End .product-action-vertical -->
                                                </figure><!-- End .product-media -->

                                                <div class="product-body">
                                                    <div class="product-cat">
                                                        <a href="#">Lighting</a>
                                                    </div><!-- End .product-cat -->
                                                    <h3 class="product-title"><a href="product.html">Carronade Table Lamp</a></h3><!-- End .product-title -->
                                                    <div class="product-price">
                                                        $499,00
                                                    </div><!-- End .product-price -->
                                                </div><!-- End .product-body -->
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>add to cart</span></a>
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .col-sm-6 col-md-4 col-lg-3 -->
                                    </div><!-- End .row -->
                                </div><!-- End .products -->
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .container -->

                    <div class="container">
                        <hr class="mt-1 mb-6">
                    </div><!-- End .container -->


        </main><!-- End .main -->

        <?php require_once __DIR__ . "/includes/footer.php"; ?>
    </div><!-- End .page-wrapper -->
    <button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i></button>

    <!-- Mobile Menu -->
    <div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

    <div class="mobile-menu-container mobile-menu-light">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-close"></i></span>

            <form action="#" method="get" class="mobile-search">
                <label for="mobile-search" class="sr-only">Search</label>
                <input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Search product ..." required>
                <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
            </form>

            <ul class="nav nav-pills-mobile nav-border-anim" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="mobile-menu-link" data-toggle="tab" href="#mobile-menu-tab" role="tab" aria-controls="mobile-menu-tab" aria-selected="true">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="mobile-cats-link" data-toggle="tab" href="#mobile-cats-tab" role="tab" aria-controls="mobile-cats-tab" aria-selected="false">Categories</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="mobile-menu-tab" role="tabpanel" aria-labelledby="mobile-menu-link">
                    <nav class="mobile-nav">
                        <ul class="mobile-menu">
                            <li class="active">
                                <a href="index.html">Home</a>

                                <ul>
                                    <li><a href="index-1.html">01 - furniture store</a></li>
                                    <li><a href="index-2.html">02 - furniture store</a></li>
                                    <li><a href="index-3.html">03 - electronic store</a></li>
                                    <li><a href="index-4.html">04 - electronic store</a></li>
                                    <li><a href="index-5.html">05 - fashion store</a></li>
                                    <li><a href="index-6.html">06 - fashion store</a></li>
                                    <li><a href="index-7.html">07 - fashion store</a></li>
                                    <li><a href="index-8.html">08 - fashion store</a></li>
                                    <li><a href="index-9.html">09 - fashion store</a></li>
                                    <li><a href="index-10.html">10 - shoes store</a></li>
                                    <li><a href="index-11.html">11 - furniture simple store</a></li>
                                    <li><a href="index-12.html">12 - fashion simple store</a></li>
                                    <li><a href="index-13.html">13 - market</a></li>
                                    <li><a href="index-14.html">14 - market fullwidth</a></li>
                                    <li><a href="index-15.html">15 - lookbook 1</a></li>
                                    <li><a href="index-16.html">16 - lookbook 2</a></li>
                                    <li><a href="index-17.html">17 - fashion store</a></li>
                                    <li><a href="index-18.html">18 - fashion store (with sidebar)</a></li>
                                    <li><a href="index-19.html">19 - games store</a></li>
                                    <li><a href="index-20.html">20 - book store</a></li>
                                    <li><a href="index-21.html">21 - sport store</a></li>
                                    <li><a href="index-22.html">22 - tools store</a></li>
                                    <li><a href="index-23.html">23 - fashion left navigation store</a></li>
                                    <li><a href="index-24.html">24 - extreme sport store</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="category.html">Shop</a>
                                <ul>
                                    <li><a href="category-list.html">Shop List</a></li>
                                    <li><a href="category-2cols.html">Shop Grid 2 Columns</a></li>
                                    <li><a href="category.html">Shop Grid 3 Columns</a></li>
                                    <li><a href="category-4cols.html">Shop Grid 4 Columns</a></li>
                                    <li><a href="category-boxed.html"><span>Shop Boxed No Sidebar<span class="tip tip-hot">Hot</span></span></a></li>
                                    <li><a href="category-fullwidth.html">Shop Fullwidth No Sidebar</a></li>
                                    <li><a href="product-category-boxed.html">Product Category Boxed</a></li>
                                    <li><a href="product-category-fullwidth.html"><span>Product Category Fullwidth<span class="tip tip-new">New</span></span></a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="wishlist.html">Wishlist</a></li>
                                    <li><a href="#">Lookbook</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="product.html" class="sf-with-ul">Product</a>
                                <ul>
                                    <li><a href="product.html">Default</a></li>
                                    <li><a href="product-centered.html">Centered</a></li>
                                    <li><a href="product-extended.html"><span>Extended Info<span class="tip tip-new">New</span></span></a></li>
                                    <li><a href="product-gallery.html">Gallery</a></li>
                                    <li><a href="product-sticky.html">Sticky Info</a></li>
                                    <li><a href="product-sidebar.html">Boxed With Sidebar</a></li>
                                    <li><a href="product-fullwidth.html">Full Width</a></li>
                                    <li><a href="product-masonry.html">Masonry Sticky Info</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Pages</a>
                                <ul>
                                    <li>
                                        <a href="about.html">About</a>

                                        <ul>
                                            <li><a href="about.html">About 01</a></li>
                                            <li><a href="about-2.html">About 02</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="contact.html">Contact</a>

                                        <ul>
                                            <li><a href="contact.html">Contact 01</a></li>
                                            <li><a href="contact-2.html">Contact 02</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="login.html">Login</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="404.html">Error 404</a></li>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="blog.html">Blog</a>

                                <ul>
                                    <li><a href="blog.html">Classic</a></li>
                                    <li><a href="blog-listing.html">Listing</a></li>
                                    <li>
                                        <a href="#">Grid</a>
                                        <ul>
                                            <li><a href="blog-grid-2cols.html">Grid 2 columns</a></li>
                                            <li><a href="blog-grid-3cols.html">Grid 3 columns</a></li>
                                            <li><a href="blog-grid-4cols.html">Grid 4 columns</a></li>
                                            <li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Masonry</a>
                                        <ul>
                                            <li><a href="blog-masonry-2cols.html">Masonry 2 columns</a></li>
                                            <li><a href="blog-masonry-3cols.html">Masonry 3 columns</a></li>
                                            <li><a href="blog-masonry-4cols.html">Masonry 4 columns</a></li>
                                            <li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Mask</a>
                                        <ul>
                                            <li><a href="blog-mask-grid.html">Blog mask grid</a></li>
                                            <li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Single Post</a>
                                        <ul>
                                            <li><a href="single.html">Default with sidebar</a></li>
                                            <li><a href="single-fullwidth.html">Fullwidth no sidebar</a></li>
                                            <li><a href="single-fullwidth-sidebar.html">Fullwidth with sidebar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="elements-list.html">Elements</a>
                                <ul>
                                    <li><a href="elements-products.html">Products</a></li>
                                    <li><a href="elements-typography.html">Typography</a></li>
                                    <li><a href="elements-titles.html">Titles</a></li>
                                    <li><a href="elements-banners.html">Banners</a></li>
                                    <li><a href="elements-product-category.html">Product Category</a></li>
                                    <li><a href="elements-video-banners.html">Video Banners</a></li>
                                    <li><a href="elements-buttons.html">Buttons</a></li>
                                    <li><a href="elements-accordions.html">Accordions</a></li>
                                    <li><a href="elements-tabs.html">Tabs</a></li>
                                    <li><a href="elements-testimonials.html">Testimonials</a></li>
                                    <li><a href="elements-blog-posts.html">Blog Posts</a></li>
                                    <li><a href="elements-portfolio.html">Portfolio</a></li>
                                    <li><a href="elements-cta.html">Call to Action</a></li>
                                    <li><a href="elements-icon-boxes.html">Icon Boxes</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav><!-- End .mobile-nav -->
                </div><!-- .End .tab-pane -->
                <div class="tab-pane fade" id="mobile-cats-tab" role="tabpanel" aria-labelledby="mobile-cats-link">
                    <nav class="mobile-cats-nav">
                        <ul class="mobile-cats-menu">
                            <li><a class="mobile-cats-lead" href="#">Daily offers</a></li>
                            <li><a class="mobile-cats-lead" href="#">Gift Ideas</a></li>
                            <li><a href="#">Beds</a></li>
                            <li><a href="#">Lighting</a></li>
                            <li><a href="#">Sofas & Sleeper sofas</a></li>
                            <li><a href="#">Storage</a></li>
                            <li><a href="#">Armchairs & Chaises</a></li>
                            <li><a href="#">Decoration </a></li>
                            <li><a href="#">Kitchen Cabinets</a></li>
                            <li><a href="#">Coffee & Tables</a></li>
                            <li><a href="#">Outdoor Furniture </a></li>
                        </ul><!-- End .mobile-cats-menu -->
                    </nav><!-- End .mobile-cats-nav -->
                </div><!-- .End .tab-pane -->
            </div><!-- End .tab-content -->

            <div class="social-icons">
                <a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .mobile-menu-wrapper -->
    </div><!-- End .mobile-menu-container -->

    <!-- Sign in / Register Modal -->
    <?php require_once __DIR__ . "/includes/modal-login-register.php" ?>
    <!-- End .modal -->

    <div class="container newsletter-popup-container mfp-hide" id="newsletter-popup-form">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="row no-gutters bg-white newsletter-popup-content">
                    <div class="col-xl-3-5col col-lg-7 banner-content-wrap">
                        <div class="banner-content text-center">
                            <img src="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png" class="logo" alt="logo" width="60" height="15">
                            <h2 class="banner-title">Ganhe <span>25<light>%</light></span> off</h2>
                            <p>Assine a newsletter para receber atualizações oportunas de seus produtos favoritos.</p>
                            <form action="#">
                                <div class="input-group input-group-round">
                                    <input type="email" class="form-control form-control-white" placeholder="Seu melhor e-mail" aria-label="Email Adress" required>
                                    <div class="input-group-append">
                                        <button class="btn" type="submit"><span>go</span></button>
                                    </div><!-- .End .input-group-append -->
                                </div><!-- .End .input-group -->
                            </form>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="register-policy-2" required>
                                <label class="custom-control-label" for="register-policy-2">Não mostre esse popup novamente</label>
                            </div><!-- End .custom-checkbox -->
                        </div>
                    </div>
                    <div class="col-xl-2-5col col-lg-5 ">
                        <img src="<?= BASE_URL ?>/source/App/pages/assets/images/popup/newsletter/img-1.jpg" class="newsletter-img" alt="newsletter">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Plugins JS File -->
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.hoverIntent.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.waypoints.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/superfish.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/owl.carousel.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.plugin.min.js"></script>

    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.magnific-popup.min.js"></script>


    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.countdown.min.js"></script>
    <!-- Main JS File -->
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/main.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/demos/demo-2.js"></script>
</body>

<script>
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: '<?= API_URL ?>/v1/produtos/todos',
            success: function(data) {
                var items = data.data;

                console.log(data);
                console.log(data.data.length);

                $('#owl-demo-1').html('<span id="testing" class="owl-carousel owl-theme"></span>');
                for (var i = 0; i < items.length; i++) {
                    $("#testing").append("<div class='product product-11 text-center'> <figure class='product-media'> <a href='ver-produto/" + items[i]['id'] + "'> <img src=' "+ items[i]['imagem_principal'] +" ' alt='Product image' class='product-image'> <img src='<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-5-2.jpg' alt='Product image' class='product-image-hover'> </a> <div class='product-action-vertical'> <a href='#' class='btn-product-icon btn-wishlist'><span>adicionar para favoritos</span></a> </div><!-- End .product-action-vertical --> </figure><!-- End .product-media --> <div class='product-body'> <h3 class='product-title'><a href='ver-produto/" + items[i]['id'] + "'>" + items[i]['nome'] + "</a></h3><!-- End .product-title --> <div class='product-price'> R$ " + items[i]['valor'] + " </div><!-- End .product-price --> <div class='product-nav product-nav-dots'> <a href='#' class='active' style='background: #74543e;'><span class='sr-only'>Color name</span></a> <a href='#' style='background: #e8e8e8;'><span class='sr-only'>Color name</span></a> </div><!-- End .product-nav --> </div><!-- End .product-body --> <div class='product-action'> <a href='ver-produto/"+items[i]['id']+" ' class='btn-product btn-cart add_cart' ><span >adicionar ao carrinho</span></a> </div><!-- End .product-action --> </div><!-- End .product -->");
                    // $("#testing").append("<div class='product product-11 text-center'> <figure class='product-media'> <a href='product.html'> <img src='<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-5-1.jpg' alt='Product image' class='product-image'> <img src='<?= BASE_URL ?>/source/App/pages/assets/images/demos/demo-2/products/product-5-2.jpg' alt='Product image' class='product-image-hover'> </a> <div class='product-action-vertical'> <a href='#' class='btn-product-icon btn-wishlist'><span>add to wishlist</span></a> </div><!-- End .product-action-vertical --> </figure><!-- End .product-media --> <div class='product-body'> <h3 class='product-title'><a href='product.html'>Petite Table Lamp</a></h3><!-- End .product-title --> <div class='product-price'> $675,00 </div><!-- End .product-price --> <div class='product-nav product-nav-dots'> <a href='#' class='active' style='background: #74543e;'><span class='sr-only'>Color name</span></a> <a href='#' style='background: #e8e8e8;'><span class='sr-only'>Color name</span></a> </div><!-- End .product-nav --> </div><!-- End .product-body --> <div class='product-action'> <a href='#' class='btn-product btn-cart'><span>add to cart</span></a> </div><!-- End .product-action --> </div><!-- End .product -->");
                };
                $(".add_cart").click(function(e) {
                    //e.preventDefault();
                    var produto_id = $(this).attr('href');
                    
                })
                var owl = $("#testing");
                owl.owlCarousel({
                    items: 7,
                    center: false,
                    responsive: {
                        0: {
                            items: 2
                        },
                        420: {
                            items: 2
                        },
                        600: {
                            items: 4
                        },
                        900: {
                            items: 5
                        },
                        1024: {
                            items: 5
                        },

                    }


                });
            },
            error: function(error) {
                console.log(msg);
            }
        });




    });
</script>




<!-- molla/index-1.html  22 Nov 2019 09:55:32 GMT -->

</html>