<div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="icon-close"></i></span>
                </button>

                <div class="form-box">
                    <div class="form-tab">
                        <ul class="nav nav-pills nav-fill" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="signin" aria-selected="true">Log In</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Registrar</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tab-content-5">
                            <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="signin-tab">
                                <form>
                                    <div class="form-group">
                                        <label for="singin-email">E-mail</label>
                                        <input type="text" class="form-control" id="singin-email" name="singin-email" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label for="singin-password">Senha *</label>
                                        <input type="password" class="form-control" id="singin-password" name="singin-password" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-footer">
                                        <button type="button" id="logar-cliente" class="btn btn-outline-primary-2">
                                            <span>LOG IN</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </button>

                                        <div class="custom-control custom-checkbox d-none">
                                            <input type="checkbox" class="custom-control-input" id="signin-remember">
                                            <label class="custom-control-label" for="signin-remember">Remember Me</label>
                                        </div><!-- End .custom-checkbox -->

                                        <a href="#" class="forgot-link d-none">Esqueceu Password?</a>
                                    </div><!-- End .form-footer -->
                                </form>
                                <div class="form-choice d-none">
                                    <p class="text-center">or sign in with</p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="#" class="btn btn-login btn-g">
                                                <i class="icon-google"></i>
                                                Login With Google
                                            </a>
                                        </div><!-- End .col-6 -->
                                        <div class="col-sm-6">
                                            <a href="#" class="btn btn-login btn-f">
                                                <i class="icon-facebook-f"></i>
                                                Login With Facebook
                                            </a>
                                        </div><!-- End .col-6 -->
                                    </div><!-- End .row -->
                                </div><!-- End .form-choice -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                                <form >
                                    <div class="form-group">
                                        <label for="register-email">E-mail *</label>
                                        <input type="email" class="form-control" id="register-email" name="re" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label for="register-email">Nome</label>
                                        <input type="text" class="form-control" id="register-nome" name="rn" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label for="register-password">Password *</label>
                                        <input type="password" class="form-control" id="register-password" name="rp" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label for="register-password">Repetir Password *</label>
                                        <input type="password" class="form-control" id="register-re-password" name="rrp" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-footer">
                                        <button id="registrar" type="submit" class="btn btn-outline-primary-2">
                                            <span>Registrar</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </button>

                                        <div class="custom-control custom-checkbox d-none">
                                            <input type="checkbox" class="custom-control-input" id="register-policy" required>
                                            <label class="custom-control-label" for="register-policy">I agree to the <a href="#">privacy policy</a> *</label>
                                        </div><!-- End .custom-checkbox -->
                                    </div><!-- End .form-footer -->
                                </form>
                                <div class="form-choice d-none">
                                    <p class="text-center">or sign in with</p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="#" class="btn btn-login btn-g">
                                                <i class="icon-google"></i>
                                                Login With Google
                                            </a>
                                        </div><!-- End .col-6 -->
                                        <div class="col-sm-6">
                                            <a href="#" class="btn btn-login  btn-f">
                                                <i class="icon-facebook-f"></i>
                                                Login With Facebook
                                            </a>
                                        </div><!-- End .col-6 -->
                                    </div><!-- End .row -->
                                </div><!-- End .form-choice -->
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .form-tab -->
                </div><!-- End .form-box -->
            </div><!-- End .modal-body -->
        </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
</div>

<!-- Plugins JS File -->
<script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#logar").click(function(e) {
            //e.preventDefault();
            alert('logado');
            //signin-modal
            $('#signin-modal').modal('hide')

        });
        $("#registrar").click(function(e) {
            //$('.validar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
            e.preventDefault();

            var name = $("#register-nome").val();
            var email = $("#register-email").val();
            //var funcao = $('select[name="funcao"]').val();
            var senha = $("#register-password").val();
            var repetir_senha = $("#register-re-password").val();

            var validaFormulario = validarFormulario(senha, repetir_senha);

            if (!validaFormulario) {
                //$('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                return false;
            }
            

            
            $.ajax({
                type: 'POST',
                url: '<?= API_URL ?>' + '/v1/cadastro/usuario-cliente',
                data: {
                    nome_completo: name,
                    email: email,
                    senha: senha,
                    funcao:912
                },

                dataType: 'json',
                success: function(data) {
                    console.log(data)
                    if (data.status == "success") {
                        //alert("Cadastrado com sucesso !");
                        $('#signin-modal').modal('hide');
                        alert('Cadastro com sucesso !');
                        //window.location.replace("<?= BASE_URL ?>");
                        
                    }
                    if (data.status == "error") {
                        //alert(data.data);
                        //$('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                    }
                    if (data.status = "validation") {
                        //alert(data.message);
                        //$('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                    }
                    

                },
                error: function(jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 422) {
                        msg = 'Error. [422]';
                        alert(jqXHR.responseJSON.data);
                        console.log(jqXHR);
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    $('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                    console.log(msg);
                },
            });
            
        });

        $("#logar-cliente").click(function (e) {
           // $('.enviar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Loading...').addClass('disabled');
            e.preventDefault();

            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: '<?=API_URL;?>' + '/v1/login-cliente',
                data: {
                    email: $("#singin-email").val(),
                    senha: $("#singin-password").val()
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    localStorage.setItem("token", data['access_token']);
                    localStorage.setItem("access", data['access']);
                    localStorage.setItem("id_user" , data['id_user']);
                    window.location.replace("<?=BASE_URL?>painel-cliente");
                    //$('.enviar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Ok').removeClass('disabled');
                },
                error: function (jqXHR, exception) {
                    //window.location.replace("<?=BASE_URL?>dashboard");
                    alert('Login ou senha incorretos');
                    $('.enviar').html('<span class="" role="status" aria-hidden="true"></span>Enviar').removeClass('disabled');
                    console.log(msg);
                    var msg = '';
                    //console.log(jqXHR.responseJSON.status)
                    if (jqXHR.responseJSON.status === 'error') {
                        msg = 'Login ou senha errado !';
                        //alert(1);
                        $('.error-login').show();
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }


                },
            });
        });


        function validarFormulario(senha, repetir_senha) {
            if (senha != repetir_senha) {
                alert('senhas devem ser iguais');
                return false;
            }
            return true;
        }
    });
</script>