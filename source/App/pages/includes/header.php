<header class="header header-2 header-intro-clearance">
            <div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <a href="tel:#"><i class="icon-phone"></i>Ligue: +55 11 9XXXX XXXX</a>
                    </div><!-- End .header-left -->

                    <div class="header-right">
                        <ul class="top-menu">
                            <li>
                                <a href="#">Links</a>
                                <ul>
                                    <li><a href="#signin-modal" data-toggle="modal">Entrar / Cadastrar</a></li>
                                </ul>
                            </li>
                        </ul><!-- End .top-menu -->
                    </div><!-- End .header-right -->
                </div><!-- End .container -->
            </div><!-- End .header-top -->

            <div class="header-middle">
                <div class="container">
                    <div class="header-left">
                        <button class="mobile-menu-toggler">
                            <span class="sr-only">Toggle mobile menu</span>
                            <i class="icon-bars"></i>
                        </button>
                        
                        <a href="<?=BASE_URL?>" class="logo" style=" float: left !important; width: 100% !important; padding: 10px !important;">
                        <img src="<?= BASE_URL ?>/source/App/files/assets/images/logo/VSOL_LOGO.png" class="header-logo" alt="Molla Logo" width="250" height="50">
                        </a>
                    </div><!-- End .header-left -->

                    <div class="header-center">
                        <div class="header-search header-search-extended header-search-visible header-search-no-radius d-none d-lg-block">
                            <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                            <form action="#" method="get">
                                <div class="header-search-wrapper search-wrapper-wide">
                                    <label for="q" class="sr-only">Procurar</label>
                                    <input type="search" class="form-control" name="q" id="q" placeholder="Procurar produto ..." required>
                                    <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                                </div><!-- End .header-search-wrapper -->
                            </form>
                        </div><!-- End .header-search -->
                    </div>

                    <div class="header-right">
                        <div class="account">
                            <a href="<?=BASE_URL?>painel-cliente" title="Minha conta">
                                <div class="icon">
                                    <i class="icon-user"></i>
                                </div>
                                <p>Conta</p>
                            </a>
                        </div><!-- End .compare-dropdown -->

                        <div class="wishlist d-none">
                            <a href="wishlist.html" title="Wishlist">
                                <div class="icon">
                                    <i class="icon-heart-o"></i>
                                    <span class="wishlist-count badge">1</span>
                                </div>
                                <p>Lista de desejos</p>
                            </a>
                        </div><!-- End .compare-dropdown -->

                        <div class="dropdown cart-dropdown">
                            <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                <div class="icon">
                                    <i class="icon-shopping-cart"></i>
                                    <span class="cart-count">0</span>
                                </div>
                                <p>Carrinho</p>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-cart-products">
                                    <div class="product">
                                        <div class="product-cart-details">
                                            <h4 class="product-title">
                                                <a href="product.html">Beige knitted elastic runner shoes</a>
                                            </h4>

                                            <span class="cart-product-info">
                                                <span class="cart-product-qty">1</span>
                                                x $84.00
                                            </span>
                                        </div><!-- End .product-cart-details -->

                                        <figure class="product-image-container">
                                            <a href="product.html" class="product-image">
                                                <img src="<?=BASE_URL?>/source/App/pages/assets/images/products/cart/product-1.jpg" alt="product">
                                            </a>
                                        </figure>
                                        <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                    </div><!-- End .product -->

                                    <div class="product">
                                        <div class="product-cart-details">
                                            <h4 class="product-title">
                                                <a href="product.html">Blue utility pinafore denim dress</a>
                                            </h4>

                                            <span class="cart-product-info">
                                                <span class="cart-product-qty">1</span>
                                                x $76.00
                                            </span>
                                        </div><!-- End .product-cart-details -->

                                        <figure class="product-image-container">
                                            <a href="product.html" class="product-image">
                                                <img src="<?=BASE_URL?>/source/App/pages/assets/images/products/cart/product-2.jpg" alt="product">
                                            </a>
                                        </figure>
                                        <a href="#" class="btn-remove" title="Remove Product"><i class="icon-close"></i></a>
                                    </div><!-- End .product -->
                                </div><!-- End .cart-product -->

                                <div class="dropdown-cart-total">
                                    <span>Total</span>

                                    <span class="cart-total-price">$160.00</span>
                                </div><!-- End .dropdown-cart-total -->

                                <div class="dropdown-cart-action">
                                    <a href="<?=BASE_URL?>carrinho" class="btn btn-primary">Ver Carrinho</a>
                                    <a href="<?=BASE_URL?>checkout" class="btn btn-outline-primary-2"><span>Checkout</span><i class="icon-long-arrow-right"></i></a>
                                </div><!-- End .dropdown-cart-total -->
                            </div><!-- End .dropdown-menu -->
                        </div><!-- End .cart-dropdown -->
                    </div><!-- End .header-right -->
                </div><!-- End .container -->
            </div><!-- End .header-middle -->

            <div class="header-bottom sticky-header">
                <div class="container mx-5">
                    <div class="header-left">
                        <div class="dropdown category-dropdown <?=$page=='pagina-inicial' ? '' : 'd-none' ?> ">
                            <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories">
                                Marcas
                            </a>

                            <div class="dropdown-menu">
                                <nav class="side-nav">
                                    <ul class="menu-vertical sf-arrows">
                                        <!--<li class="item-lead"><a href="#">Daily offers</a></li>
                                        <li class="item-lead"><a href="#">Gift Ideas</a></li>-->
                                        <li><a href="#">Driven</a></li>
                                        <li><a href="#">Vaper Shot</a></li>
                                        <li><a href="#">Tranquil</a></li>
                                        <li><a href="#">Vaporesso</a></li>
                                        <!--<li><a href="#">Armchairs & Chaises</a></li>
                                        <li><a href="#">Decoration </a></li>
                                        <li><a href="#">Kitchen Cabinets</a></li>
                                        <li><a href="#">Coffee & Tables</a></li>
                                        <li><a href="#">Outdoor Furniture </a></li>-->
                                    </ul><!-- End .menu-vertical -->
                                </nav><!-- End .side-nav -->
                            </div><!-- End .dropdown-menu -->
                        </div><!-- End .category-dropdown -->
                    </div><!-- End .header-left -->

                    <div class="header-center">
                        <nav class="main-nav">
                            <ul class="menu sf-arrows">
                                <li>
                                    <a href="#" class="sf-with-ul">Home</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sf-with-ul">Lançamentos</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sf-with-ul">Aparelhos</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" class="sf-with-ul">Líquidos</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="blog.html" class="sf-with-ul">PODs & Descartáveis</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="blog.html" class="sf-with-ul">Vaporizadores</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="blog.html" class="sf-with-ul">Acessórios</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="blog.html" class="sf-with-ul">Outlet</a>
                                    <ul>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 01</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 02</a>
                                        </li>
                                        <li>
                                            <a href="about.html" class="sf-with-ul">vaper 03</a>
                                        </li>
                                    </ul>
                                </li>
                                    
                            </ul><!-- End .menu -->
                        </nav><!-- End .main-nav -->
                    </div><!-- End .header-center -->

                    <!--

                    <div class="header-right">
                        <i class="la la-lightbulb-o"></i><p>Liquidação<span class="highlight">&nbsp;até 30% de desconto</span></p>
                    </div>
                    -->
                </div><!-- End .container -->
            </div><!-- End .header-bottom -->
        </header><!-- End .header -->

        <!-- Plugins JS File -->
<script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        if (localStorage.getItem("user_id") === null) {
           // alert("sem id usuario");
        }
       
       // alert()
        
    });
</script>