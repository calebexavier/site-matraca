<footer class="footer footer-2">
            <div class="icon-boxes-container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon text-dark">
                                    <i class="icon-rocket"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Entregamos em todo brasil</h3><!-- End .icon-box-title -->
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-sm-6 col-lg-3 -->

                        <div class="col-sm-6 col-lg-3">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon text-dark">
                                    <i class="icon-rotate-left"></i>
                                </span>

                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Despacho rápido após a confirmação de pagamento</h3><!-- End .icon-box-title -->
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-sm-6 col-lg-3 -->

                        <div class="col-sm-6 col-lg-3">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon text-dark">
                                    <i class="icon-info-circle"></i>
                                </span>

                                <div class="icon-box-content">
                                    <h3 class="icon-box-title"> Ambiente 100% seguro</h3><!-- End .icon-box-title -->
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-sm-6 col-lg-3 -->

                        <div class="col-sm-6 col-lg-3">
                            <div class="icon-box icon-box-side">
                                <span class="icon-box-icon text-dark">
                                    <i class="icon-life-ring"></i>
                                </span>

                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Parcelamento no cartão</h3><!-- End .icon-box-title -->
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-sm-6 col-lg-3 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .icon-boxes-container -->

        	<div class="footer-newsletter bg-image" style="background-image: url(<?=BASE_URL?>/source/App/pages/assets/images/backgrounds/GetTheLatest.jpg)">
        		<div class="container">
        			<div class="heading text-center">
                        <h3 class="title">Receba as últimas ofertas</h3><!-- End .title -->
                    </div><!-- End .heading -->

                    <div class="row">
                    	<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                            <form action="#">
    							<div class="input-group">
    								<input type="email" class="form-control" placeholder="Digite seu endereço de e-mail" aria-label="Email Adress" aria-describedby="newsletter-btn" required>
    								<div class="input-group-append">
    									<button class="btn btn-primary" type="submit" id="newsletter-btn"><span>Se inscrever</span><i class="icon-long-arrow-right"></i></button>
    								</div><!-- .End .input-group-append -->
    							</div><!-- .End .input-group -->
                            </form>
                    	</div><!-- End .col-sm-10 offset-sm-1 col-lg-6 offset-lg-3 -->
                    </div><!-- End .row -->
        		</div><!-- End .container -->
        	</div><!-- End .footer-newsletter bg-image -->

        	<div class="footer-middle">
	            <div class="container">
	            	<div class="row">
	            		<div class="col-sm-12 col-lg-6">
	            			<div class="widget widget-about">
	            				<img src="<?= BASE_URL ?>/source/App/files/assets/images/logo/VSOL_LOGO.png" class="footer-logo" alt="Footer Logo" width="120" height="30">
	            				<p class="text-justify">Entre no melhor site de Cigarro Eletrônico, Vaporizador de Ervas, Pod System e Líquidos.  Temos Cigarro Eletrônico com qualidade e preço baixo! Trabalhamos apenas com VAPE Original para comprar com pronta entrega! </p>
	            				
	            				<div class="widget-about-info">
	            					<div class="row">
	            						<div class="col-sm-6 col-md-4">
	            							<span class="widget-about-title">Alguma dúvida? Ligue: </span>
	            							<a href="tel:123456789">+55 11 9XXXX XXXX</a>
	            						</div><!-- End .col-sm-6 -->
	            						<div class="col-sm-6 col-md-8">
	            							<span class="widget-about-title">Métodos de pagamento</span>
	            							<figure class="footer-payments">
							        			<img src="<?=BASE_URL?>/source/App/pages/assets/images/payments.png" alt="Payment methods" width="272" height="20">
							        		</figure><!-- End .footer-payments -->
	            						</div><!-- End .col-sm-6 -->
	            					</div><!-- End .row -->
	            				</div><!-- End .widget-about-info -->
	            			</div><!-- End .widget about-widget -->
	            		</div><!-- End .col-sm-12 col-lg-3 -->

	            		<div class="col-sm-4 col-lg-2">
	            			<div class="widget">
	            				<h4 class="widget-title">Categorias</h4><!-- End .widget-title -->
	            				
								<ul class="widget-list">
	            					<li><a href="#">Home</a></li>
	            					<li><a href="#">Lançamentos</a></li>
	            					<li><a href="#">Aparelhos</a></li>
	            					<li><a href="#">Líquidos</a></li>
									<li><a href="#">PODs & Descartáveis</a></li>
									<li><a href="#">Vaporizadores</a></li>
									<li><a href="#">Acessórios</a></li>
									<li><a href="#">Líquidos</a></li>
									<li><a href="#">Outlet</a></li>
	            				</ul><!-- End .widget-list -->
	            			</div><!-- End .widget -->
	            		</div><!-- End .col-sm-4 col-lg-3 -->

	            		<div class="col-sm-4 col-lg-2">
	            			<div class="widget">
	            				<h4 class="widget-title">Conteúdo</h4><!-- End .widget-title -->
							
	            				<ul class="widget-list">
	            					<li><a href="#">Fale conosco</a></li>
	            					<li><a href="#">Como comprar</a></li>
	            					<li><a href="#">O que é cigarro eletrônico</a></li>
	            					<li><a href="#">Política de troca e devolução</a></li>
									<li><a href="#">Prazo de entrega</a></li>
	            				</ul><!-- End .widget-list -->
	            			</div><!-- End .widget -->
	            		</div><!-- End .col-sm-4 col-lg-3 -->

	            		<div class="col-sm-4 col-lg-2">
	            			<div class="widget">
	            				<h4 class="widget-title">Sobre a loja</h4><!-- End .widget-title -->
						
	            				<ul class="widget-list">
	            					<li><a href="#">Cigarro Eletrônico</a></li>
	            					<li><a href="#">Vaper Stock</a></li>
	            					<li><a href="#">loja virtual: Brasília</a></li>
	            				</ul><!-- End .widget-list -->
	            			</div><!-- End .widget -->
	            		</div><!-- End .col-sm-64 col-lg-3 -->
	            	</div><!-- End .row -->
	            </div><!-- End .container -->
	        </div><!-- End .footer-middle -->

	        <div class="footer-bottom">
	        	<div class="container">
	        		<p class="footer-copyright">Copyright © 2020 VaperStock. Todos direitos reservados.</p><!-- End .footer-copyright -->
	        		<ul class="footer-menu">
	        			<li><a href="#">Termos de Uso</a></li>
	        			<li><a href="#">Politica de privacidade</a></li>
	        		</ul><!-- End .footer-menu -->

	        		<div class="social-icons social-icons-color">
    					<a href="#" class="social-icon social-facebook" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
    					<a href="#" class="social-icon social-instagram" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
						<a href="#" class="#" title="Email">Email: xxx@vaperstock.com.br</a>
    				</div><!-- End .soial-icons -->
	        	</div><!-- End .container -->
	        </div><!-- End .footer-bottom -->
        </footer><!-- End .footer -->