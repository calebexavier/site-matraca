<!DOCTYPE html>
<html lang="en">


<!-- molla/dashboard.html  22 Nov 2019 10:03:13 GMT -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Matraca - eCommerce</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Matraca - eCommerce">
    <meta name="author" content="p-themes">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <link rel="icon" type="image/png" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <link rel="manifest" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/site.html">
    <link rel="mask-icon" href="<?= BASE_URL ?>/source/App/pages/assets/images/icons/safari-pinned-tab.svg" color="#666666">
    <link rel="shortcut icon" href="<?= BASE_URL ?>/source/App/files/assets/images/logo/Logo105x25.png">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="<?= BASE_URL ?>/source/App/pages/assets/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/bootstrap.min.css">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="<?= BASE_URL ?>/source/App/pages/assets/css/style.css">
</head>

<body>
    <div class="page-wrapper">
        <?php require __DIR__ . "/includes/header.php" ?>
        <!-- End .header -->

        <main class="main">
            <div class="page-header text-center" style="background-image: url('<?= BASE_URL ?>/source/App/pages/assets/images/page-header-bg.jpg')">
                <div class="container">
                    <h1 class="page-title">Minha conta<span>Compras</span></h1>
                </div><!-- End .container -->
            </div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Shop</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Minha conta</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div class="dashboard">
                    <div class="container">
                        <div class="row">
                            <aside class="col-md-4 col-lg-3">
                                <ul class="nav nav-dashboard flex-column mb-3 mb-md-0" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab-dashboard-link" data-toggle="tab" href="#tab-dashboard" role="tab" aria-controls="tab-dashboard" aria-selected="true">Dashboard</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab-orders-link" data-toggle="tab" href="#tab-orders" role="tab" aria-controls="tab-orders" aria-selected="false">Pedidos</a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a class="nav-link" id="tab-downloads-link" data-toggle="tab" href="#tab-downloads" role="tab" aria-controls="tab-downloads" aria-selected="false">Downloads</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" id="tab-address-link" data-toggle="tab" href="#tab-address" role="tab" aria-controls="tab-address" aria-selected="false">Endereço</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab-account-link" data-toggle="tab" href="#tab-account" role="tab" aria-controls="tab-account" aria-selected="false">Detalhes da conta</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link logout" href="#">Log Out</a>
                                    </li>
                                </ul>
                            </aside><!-- End .col-lg-3 -->

                            <div class="col-md-8 col-lg-9">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab-dashboard" role="tabpanel" aria-labelledby="tab-dashboard-link">
                                        <p>Olá <span class="font-weight-normal text-dark user_name"></span> <span class="font-weight-normal text-dark"></span>? <a href="" class="logout">Log out</a>)
                                            <br>
                                            No painel da sua conta, você pode visualizar seus <a href="#tab-orders" class="tab-trigger-link link-underline">pedidos recentes</a>, gerencie seus <a href="#tab-address" class="tab-trigger-link">endereços de entrega e cobrança</a>, e <a href="#tab-account" class="tab-trigger-link">edite seu password e detalhes da conta</a>.</p>
                                    </div><!-- .End .tab-pane -->

                                    <div class="tab-pane fade" id="tab-orders" role="tabpanel" aria-labelledby="tab-orders-link">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-cart table-mobile">
                                                    <thead>
                                                        <tr>
                                                            <th>Pedido</th>
                                                            
                                                            <th>Total</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="lista-pedidos">


                                                    </tbody>
                                                </table><!-- End .table table-wishlist -->
                                            </div>
                                        </div>

                                        <a href="category.html" class="btn btn-outline-primary-2"><span>IR para eCommerce</span><i class="icon-long-arrow-right"></i></a>
                                    </div><!-- .End .tab-pane -->

                                    <div class="tab-pane fade d-none" id="tab-downloads" role="tabpanel" aria-labelledby="tab-downloads-link">
                                        <p>No downloads available yet.</p>
                                        <a href="category.html" class="btn btn-outline-primary-2"><span>GO SHOP</span><i class="icon-long-arrow-right"></i></a>
                                    </div><!-- .End .tab-pane -->

                                    <div class="tab-pane fade" id="tab-address" role="tabpanel" aria-labelledby="tab-address-link">
                                        <p>Seu endereço paras as compras no site.</p>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card card-dashboard">
                                                    <div class="card-body">
                                                        <h3 class="card-title">Endereço principal</h3><!-- End .card-title -->

                                                        <p>João Almeida<br>
                                                            Empresa de sapatos<br>
                                                            Goiânia<br>
                                                            Brasil, BR Goiás<br>
                                                            74140140<br>
                                                            joao@gmail.com<br>
                                                            <a href="#">Edit <i class="icon-edit"></i></a></p>
                                                    </div><!-- End .card-body -->
                                                </div><!-- End .card-dashboard -->
                                            </div><!-- End .col-lg-6 -->

                                            <div class="col-lg-6 d-none">
                                                <div class="card card-dashboard">
                                                    <div class="card-body">
                                                        <h3 class="card-title">Shipping Address</h3><!-- End .card-title -->

                                                        <p>You have not set up this type of address yet.<br>
                                                            <a href="#">Edit <i class="icon-edit"></i></a></p>
                                                    </div><!-- End .card-body -->
                                                </div><!-- End .card-dashboard -->
                                            </div><!-- End .col-lg-6 -->
                                        </div><!-- End .row -->
                                    </div><!-- .End .tab-pane -->

                                    <div class="tab-pane fade" id="tab-account" role="tabpanel" aria-labelledby="tab-account-link">
                                        <form action="#">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Nome Completo *</label>
                                                    <input id="nome_completo" type="text" class="form-control" required>
                                                </div><!-- End .col-sm-6 -->


                                            </div><!-- End .row -->



                                            <label>Email *</label>
                                            <input id="email" type="email" readonly class="form-control" required>



                                            <label>Novo password (deixe em branco para manter o password atual)</label>
                                            <input id="senha" type="password" class="form-control">

                                            <label>Repetir novo password</label>
                                            <input id="repetir_senha" type="password" class="form-control mb-2">

                                            <button id="atualizar_dados" type="submit" class="btn btn-outline-primary-2">
                                                <span>Atualizar</span>
                                                <i class="icon-long-arrow-right"></i>
                                            </button>
                                        </form>
                                    </div><!-- .End .tab-pane -->
                                </div>
                            </div><!-- End .col-lg-9 -->
                        </div><!-- End .row -->
                    </div><!-- End .container -->
                </div><!-- End .dashboard -->
            </div><!-- End .page-content -->
        </main><!-- End .main -->

        <?php require __DIR__ . "/includes/footer.php" ?>
        <!-- End .footer -->
    </div><!-- End .page-wrapper -->
    <button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i></button>

    <!-- Mobile Menu -->
    <div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

    <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-close"></i></span>

            <form action="#" method="get" class="mobile-search">
                <label for="mobile-search" class="sr-only">Search</label>
                <input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Search in..." required>
                <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
            </form>

            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li class="active">
                        <a href="index.html">Home</a>

                        <ul>
                            <li><a href="index-1.html">01 - furniture store</a></li>
                            <li><a href="index-2.html">02 - furniture store</a></li>
                            <li><a href="index-3.html">03 - electronic store</a></li>
                            <li><a href="index-4.html">04 - electronic store</a></li>
                            <li><a href="index-5.html">05 - fashion store</a></li>
                            <li><a href="index-6.html">06 - fashion store</a></li>
                            <li><a href="index-7.html">07 - fashion store</a></li>
                            <li><a href="index-8.html">08 - fashion store</a></li>
                            <li><a href="index-9.html">09 - fashion store</a></li>
                            <li><a href="index-10.html">10 - shoes store</a></li>
                            <li><a href="index-11.html">11 - furniture simple store</a></li>
                            <li><a href="index-12.html">12 - fashion simple store</a></li>
                            <li><a href="index-13.html">13 - market</a></li>
                            <li><a href="index-14.html">14 - market fullwidth</a></li>
                            <li><a href="index-15.html">15 - lookbook 1</a></li>
                            <li><a href="index-16.html">16 - lookbook 2</a></li>
                            <li><a href="index-17.html">17 - fashion store</a></li>
                            <li><a href="index-18.html">18 - fashion store (with sidebar)</a></li>
                            <li><a href="index-19.html">19 - games store</a></li>
                            <li><a href="index-20.html">20 - book store</a></li>
                            <li><a href="index-21.html">21 - sport store</a></li>
                            <li><a href="index-22.html">22 - tools store</a></li>
                            <li><a href="index-23.html">23 - fashion left navigation store</a></li>
                            <li><a href="index-24.html">24 - extreme sport store</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="category.html">Shop</a>
                        <ul>
                            <li><a href="category-list.html">Shop List</a></li>
                            <li><a href="category-2cols.html">Shop Grid 2 Columns</a></li>
                            <li><a href="category.html">Shop Grid 3 Columns</a></li>
                            <li><a href="category-4cols.html">Shop Grid 4 Columns</a></li>
                            <li><a href="category-boxed.html"><span>Shop Boxed No Sidebar<span class="tip tip-hot">Hot</span></span></a></li>
                            <li><a href="category-fullwidth.html">Shop Fullwidth No Sidebar</a></li>
                            <li><a href="product-category-boxed.html">Product Category Boxed</a></li>
                            <li><a href="product-category-fullwidth.html"><span>Product Category Fullwidth<span class="tip tip-new">New</span></span></a></li>
                            <li><a href="cart.html">Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                            <li><a href="wishlist.html">Wishlist</a></li>
                            <li><a href="#">Lookbook</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="product.html" class="sf-with-ul">Product</a>
                        <ul>
                            <li><a href="product.html">Default</a></li>
                            <li><a href="product-centered.html">Centered</a></li>
                            <li><a href="product-extended.html"><span>Extended Info<span class="tip tip-new">New</span></span></a></li>
                            <li><a href="product-gallery.html">Gallery</a></li>
                            <li><a href="product-sticky.html">Sticky Info</a></li>
                            <li><a href="product-sidebar.html">Boxed With Sidebar</a></li>
                            <li><a href="product-fullwidth.html">Full Width</a></li>
                            <li><a href="product-masonry.html">Masonry Sticky Info</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Pages</a>
                        <ul>
                            <li>
                                <a href="about.html">About</a>

                                <ul>
                                    <li><a href="about.html">About 01</a></li>
                                    <li><a href="about-2.html">About 02</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="contact.html">Contact</a>

                                <ul>
                                    <li><a href="contact.html">Contact 01</a></li>
                                    <li><a href="contact-2.html">Contact 02</a></li>
                                </ul>
                            </li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="faq.html">FAQs</a></li>
                            <li><a href="404.html">Error 404</a></li>
                            <li><a href="coming-soon.html">Coming Soon</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="blog.html">Blog</a>

                        <ul>
                            <li><a href="blog.html">Classic</a></li>
                            <li><a href="blog-listing.html">Listing</a></li>
                            <li>
                                <a href="#">Grid</a>
                                <ul>
                                    <li><a href="blog-grid-2cols.html">Grid 2 columns</a></li>
                                    <li><a href="blog-grid-3cols.html">Grid 3 columns</a></li>
                                    <li><a href="blog-grid-4cols.html">Grid 4 columns</a></li>
                                    <li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Masonry</a>
                                <ul>
                                    <li><a href="blog-masonry-2cols.html">Masonry 2 columns</a></li>
                                    <li><a href="blog-masonry-3cols.html">Masonry 3 columns</a></li>
                                    <li><a href="blog-masonry-4cols.html">Masonry 4 columns</a></li>
                                    <li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Mask</a>
                                <ul>
                                    <li><a href="blog-mask-grid.html">Blog mask grid</a></li>
                                    <li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Single Post</a>
                                <ul>
                                    <li><a href="single.html">Default with sidebar</a></li>
                                    <li><a href="single-fullwidth.html">Fullwidth no sidebar</a></li>
                                    <li><a href="single-fullwidth-sidebar.html">Fullwidth with sidebar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="elements-list.html">Elements</a>
                        <ul>
                            <li><a href="elements-products.html">Products</a></li>
                            <li><a href="elements-typography.html">Typography</a></li>
                            <li><a href="elements-titles.html">Titles</a></li>
                            <li><a href="elements-banners.html">Banners</a></li>
                            <li><a href="elements-product-category.html">Product Category</a></li>
                            <li><a href="elements-video-banners.html">Video Banners</a></li>
                            <li><a href="elements-buttons.html">Buttons</a></li>
                            <li><a href="elements-accordions.html">Accordions</a></li>
                            <li><a href="elements-tabs.html">Tabs</a></li>
                            <li><a href="elements-testimonials.html">Testimonials</a></li>
                            <li><a href="elements-blog-posts.html">Blog Posts</a></li>
                            <li><a href="elements-portfolio.html">Portfolio</a></li>
                            <li><a href="elements-cta.html">Call to Action</a></li>
                            <li><a href="elements-icon-boxes.html">Icon Boxes</a></li>
                        </ul>
                    </li>
                </ul>
            </nav><!-- End .mobile-nav -->

            <div class="social-icons">
                <a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>
                <a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .mobile-menu-wrapper -->
    </div><!-- End .mobile-menu-container -->

    <!-- Sign in / Register Modal -->
    <?php require_once __DIR__ . "/includes/modal-login-register.php" ?>
    <!-- End .modal -->

    <!-- Plugins JS File -->
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.hoverIntent.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.waypoints.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/superfish.min.js"></script>
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/owl.carousel.min.js"></script>
    <!-- Main JS File -->
    <script src="<?= BASE_URL ?>/source/App/pages/assets/js/main.js"></script>
</body>


<!-- molla/dashboard.html  22 Nov 2019 10:03:13 GMT -->

</html>

<script src="<?= BASE_URL ?>/source/App/pages/assets/js/jquery.min.js"></script>
<script>
    $(document).ready(function() {

        $(".logout").click(function() {
            localStorage.clear();
            window.location.replace("<?= BASE_URL ?>");
        });


        if (localStorage.getItem("id_user") === null) {
            window.location.replace("<?= BASE_URL ?>");
        } else {
            var id_user = localStorage.getItem("id_user");

            $.ajax({
                type: 'GET',
                url: '<?= API_URL ?>' + '/v1/cadastro/' + id_user,
                data: {
                    id: id_user
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'))
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 'Token is Expired') {
                        localStorage.clear();
                        window.location.href = "<?= BASE_URL ?>";
                    }
                    $(".user_name").html(data.nome_completo);
                    $("#email").val(data.email);
                    $("#nome_completo").val(data.nome_completo);
                    /*
                    if (data.funcao.id == "2") {
                        $('select[name="funcao"]').val("administrador");
                    }
                    if (data.funcao.id == "1") {
                        $('select[name="funcao"]').val("vendedor");
                    }
                    */

                    $("#senha").val(data.senha);
                    $("#repetir_senha").val(data.senha);

                    console.log(data);

                },
                error: function(jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }

                    console.log(msg);
                    // window.location.replace("<?= BASE_URL ?>");
                },
            });
            var pedidos ="";

            $.ajax({
                type: 'GET',
                url: '<?= API_URL ?>' + '/v1/mostrarPedidosPorUsuario/' + id_user,
                data: {
                    id: id_user
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'))
                },
                dataType: 'json',
                success: function(data) {
                    for(var i = 0 ; i<data.length ; i++){
                        pedidos += '   <tr>  ' +
                '   											<td class="product-col">  #' + data[i].id + ' ' +
                '   											</td>  ' +
                '   											<td class="price-col">R$ ' + data[i].total + '</td>  ' +

                '  										</tr>  ';

                    }

                    $("#lista-pedidos").html(pedidos);

                    console.log(data);

                },
                error: function(jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }

                    console.log(msg);
                    // window.location.replace("<?= BASE_URL ?>");
                },
            });

        }

        $("#atualizar_dados").click(function(e) {
            //$('.validar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
            e.preventDefault();

            var name = $("#nome_completo").val();
            var email = $("#email").val();
            var funcao = "3";
            var senha = $("#senha").val();
            var repetir_senha = $("#repetir_senha").val();

            var validaFormulario = validarFormulario(senha, repetir_senha);

            if (!validaFormulario) {
                // $('.validar').html('<span class="" role="status" aria-hidden="true"></span>Editar').removeClass('disabled');
                return false;
            }



            $.ajax({
                type: 'PUT',
                url: '<?= API_URL ?>' + '/v1/cadastro/' + id_user,
                data: {
                    nome: name,
                    email: email,
                    funcao: funcao,
                    senha: senha,
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'))
                },

                dataType: 'json',
                success: function(data) {
                    console.log(data)
                    if (data.status == "success") {
                        //alert("Editado com sucesso !");
                        window.location.replace("<?= BASE_URL ?>painel-cliente");
                    }
                    if (data.status = "error") {
                        // alert(data.data);
                        //$('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                    }
                    if (data.status = "validation") {
                        alert(data.message);
                        //$('.validar').html('<span class="" role="status" aria-hidden="true"></span>Cadastrar').removeClass('disabled');
                    }

                },
                error: function(jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    $('.validar').html('<span class="" role="status" aria-hidden="true"></span>Editar').removeClass('disabled');
                    console.log(msg);
                },
            });
        });


        function validarFormulario(senha, repetir_senha) {
            if (senha != repetir_senha) {
                alert('senhas devem ser iguais');
                return false;
            }
            return true;
        }

    });
</script>